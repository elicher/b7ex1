import React, { useState } from "react";


const App = () => {

  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("")
  const [content,setContent] = useState("")
  const [stringColor,setColor] = useState("")

  const handleEmail = (e) => {
    setEmail(e.target.value)
    console.log(email)
  }
  const handlePassword = (e) => {
    setPassword(e.target.value)
    console.log(password)
  }

  const handleConfirmPassword = (e) => {
    setConfirmPassword(e.target.value)
    console.log(confirmPassword)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(email.includes("@"))
    if (email.includes("@@")==true||email.includes("@")==false) {
      setContent("Please, provide a valid email.")
      setColor("red")
    } else if (password.length<=7) {
      setContent("Password must have at least 8 characters.")
      setColor("red")
  } else if (password!==confirmPassword) {
    setContent ("Passwords should match.")
    setColor("red")
  } else {
    setContent("Successfully registered")
    setColor("green")
  }}

  return (
    <div className="App">
    <h1> Registration </h1>
    <form onSubmit={handleSubmit}>
      <h2> Type your email</h2>
      <input name="mail" onChange={handleEmail} />
      <h2> Type your password</h2>
      <input name="password" onChange={handlePassword} type="password"/>
      <h2> Confirm password</h2>
      <input name="confirmPassword" onChange={handleConfirmPassword}type="password"/>
      <button>Submit</button>
    </form>
    <div>
        <h2 style={{color: stringColor}}>{ content }</h2>
      </div>
    </div>
  )
}

export default App;