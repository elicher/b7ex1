import { configure, mount } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { act } from "@testing-library/react";

configure({ adapter: new Adapter() });

const app = mount(<App />);

describe("b7ex01: Form Validation", () => {
  it("In a functional App.js there should be a form with 3 inputs (each with an onChange event updating state) and a button", () => {
    const form = app.find("form");
    const button = app.find("button");
    const inputs = app.find("input");

    let allGood = false;

    inputs.forEach((input) => {
      input.prop("onChange") !== undefined
        ? (allGood = true)
        : (allGood = false);
    });

    // form.prop("onChange") !== undefined ? (allGood = true) : (allGood = false);

    if (!form || !button || !allGood || inputs.length !== 3) return false;
  });
  it("No message should be displayed initially", () => {
    const message = app.find("h2");
    if (message.text() !== "") return false;
  });
  it("After hitting submit there should be a red message 'Please, provide a valid email'", () => {
    const button = app.find("button");
    button.simulate("click");

    const message = app.find("h2");
    if (
      message.text() !== "Please, provide a valid email" ||
      message.props().style.color !== "red"
    )
      return false;
  });
  it("After typing two @@ in the email input there should be a red message 'Please, provide a valid email'", () => {
    const inputs = app.find("input");
    inputs.forEach((input) => {
      if (input.props().name === "email") {
        input.simulate("change", { target: { value: "@@", name: "email" } });
      }
    });

    const button = app.find("button");
    button.simulate("click");

    const message = app.find("h2");
    if (
      message.text() !== "Please, provide a valid email" ||
      message.props().style.color !== "red"
    )
      return false;
  });
  it("After typing a correct email and a password with less than 8 characters there should be a red message 'Password must have at least 8 characters'", () => {
    cleanUpInputs();
    const inputs = app.find("input");
    inputs.forEach((input) => {
      input.props().name === "email"
        ? input.simulate("change", {
            target: { value: "mail@mail.com", name: "email" },
          })
        : input.props().name === "password" &&
          input.simulate("change", {
            target: { value: "1234567", name: "password" },
          });
    });

    const button = app.find("button");
    button.simulate("click");

    const message = app.find("h2");

    if (
      message.text() !== "Password must have at least 8 characters" ||
      message.props().style.color !== "red"
    )
      return false;
  });
  it("By typing two passwords longer than 8 characters but without matching there should be a red message 'Passwords should match", () => {
    cleanUpInputs();
    const inputs = app.find("input");
    inputs.forEach((input) => {
      input.props().name === "email"
        ? input.simulate("change", {
            target: { value: "mail@mail.com", name: "email" },
          })
        : input.props().name === "password"
        ? input.simulate("change", {
            target: { value: "12345678", name: "password" },
          })
        : input.props().name === "password2" &&
          input.simulate("change", {
            target: { value: "01234567", name: "password2" },
          });
    });

    const button = app.find("button");
    button.simulate("click");

    const message = app.find("h2");

    if (
      message.text() !== "Passwords should match" ||
      message.text() === "Please, provide a valid email" ||
      message.text() === "Password must have at least 8 characters" ||
      message.props().style.color !== "red"
    )
      return false;
  });
  it("By typing everything correctly there should be a green message 'Successfully registered", () => {
    cleanUpInputs();
    const inputs = app.find("input");
    inputs.forEach((input) => {
      input.props().name === "email"
        ? input.simulate("change", {
            target: { value: "mail@mail.com", name: "email" },
          })
        : input.props().name === "password"
        ? input.simulate("change", {
            target: { value: "12345678", name: "password" },
          })
        : input.props().name === "password2" &&
          input.simulate("change", {
            target: { value: "12345678", name: "password2" },
          });
    });

    const button = app.find("button");
    button.simulate("click");

    const message = app.find("h2");
    if (
      message.text() !== "Successfully registered" ||
      message.props().style.color !== "green"
    )
      return false;
  });
  it("Message should be empty string after 2 seconds", async () => {
    await act(async () => {
      await new Promise((resolve) => setTimeout(resolve, 2100));
      app.update();
    });
    expect(app.find("h2").text()).toBe("");
  });
});

const cleanUpInputs = () => {
  const inputs = app.find("input");
  inputs.forEach((input) =>
    input.props().name === "email"
      ? input.simulate("change", { target: { value: "", name: "email" } })
      : input.props().name === "password"
      ? input.simulate("change", { target: { value: "", name: "password" } })
      : input.props().name === "password2" &&
        input.simulate("change", { target: { value: "", name: "password2" } })
  );
};
